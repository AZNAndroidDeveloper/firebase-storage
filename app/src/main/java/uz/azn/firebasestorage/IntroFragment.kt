package uz.azn.firebasestorage

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import uz.azn.firebasestorage.databinding.FragmentIntroBinding
import java.io.ByteArrayOutputStream
import java.util.*

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var firebaseStorage: FirebaseStorage
    private lateinit var storageReference: StorageReference
    private lateinit var binding: FragmentIntroBinding
    private val CAMERA_REQUEST = 0
    private val GALLERY_REQUEST = 1
    private var bitmap: Bitmap? = null
    private var image_id = 1
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        firebaseStorage = FirebaseStorage.getInstance()
        storageReference = firebaseStorage.reference.child("image/image${image_id}")
        requestPermisson()
        checkPermissionExternalStorage()
        downloadImage("image1")
        binding.imageView1.setOnClickListener {
            val alertDialog = AlertDialog.Builder(requireContext())
                .setItems(
                    arrayOf("Camera", "Gallery", "Cancel")
                ) { dialog, which ->
                    if (which == 0) {
                        val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(intent, CAMERA_REQUEST)
                    } else if (which == 1) {
                        val intent = Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                        )
                        startActivityForResult(intent, GALLERY_REQUEST)

                    } else {
                        dialog.dismiss()
                    }
                }
            alertDialog.show()


        }
        binding.btnDownloading.setOnClickListener {
            downloadImage("image1")
        }
    }

    private fun downloadImage(file: String) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val downloadRef =  firebaseStorage.reference.child("image/$file")
            val maxDownloadFileSize = 5L * 1024 * 1024
            val byte = downloadRef.getBytes(maxDownloadFileSize).await()
            val bitmap = BitmapFactory.decodeByteArray(byte, 0, byte.size)
            withContext(Dispatchers.Main) {
                binding.imageView2.setImageBitmap(bitmap)
            }
        } catch (ex: Exception) {
            withContext(Dispatchers.Main) {
                Toast.makeText(requireContext(), "Something is wrong", Toast.LENGTH_SHORT).show()
            }
        }
    }

    @SuppressLint("Recycle")
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_CANCELED) {
            when (requestCode) {
                CAMERA_REQUEST -> {
                    bitmap = data?.extras?.get("data") as Bitmap
                }
                GALLERY_REQUEST -> {
                    val uri = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor =
                        activity!!.contentResolver.query(uri!!, filePathColumn, null, null,null)
                    cursor!!.moveToFirst()
                    val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
                    val filePath = cursor?.getString(columnIndex)
                    bitmap = BitmapFactory.decodeFile(filePath)

                }

            }
            uploadingImage()
            binding.imageView1.setImageBitmap(bitmap)

        }
    }

    fun checkPermissionExternalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result =
                requireActivity().checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }

    fun requestPermisson() {
        try {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE),
                100
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun uploadingImage(){

        val progressDialog = ProgressDialog(requireContext())
        progressDialog.setMessage("Uploading...")
        progressDialog.setCancelable(false)
        progressDialog.show()
        val byteArrayOutputStream = ByteArrayOutputStream()
        // bu rasm farmatini kichraytirish uchun ishlatiladi
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        storageReference.putBytes(byteArray).addOnSuccessListener { taskSnapshot ->
            if (taskSnapshot.task.isSuccessful) {
                progressDialog.dismiss()
                Toast.makeText(
                    requireContext(),
                    "Uploading successfully",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                progressDialog.dismiss()
                Toast.makeText(
                    requireContext(),
                    "Something is wrong",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}